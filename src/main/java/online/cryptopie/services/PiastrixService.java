package online.cryptopie.services;

import online.cryptopie.models.transaction.Transaction;
import org.springframework.web.client.ResourceAccessException;

public interface PiastrixService {
    void saveTransaction(Transaction transaction);

    void declineTransaction(Transaction transaction, String errorCause);

    void withdraw(Transaction transaction) throws ResourceAccessException;

    void checkTransaction(Transaction transaction) throws ResourceAccessException;

    void ping() throws ResourceAccessException;
}
