package online.cryptopie.app;

import java.io.File;
import java.io.IOException;

public class Locker {
    public static final String PING_LOCK = "ping.lock";
    public static final String PAY_LOCK = "pay.lock";
    public static final String CONFIRM_LOCK = "confirm.lock";
    private static final String PATH = "";//"/home/admin/"
    private static Locker instance;

    private Locker() {
    }

    public static synchronized Locker getInstance() {
        if (instance == null) {
            instance = new Locker();
        }
        return instance;
    }

    public boolean isNotLocked(String lockType) {
        File file = new File(String.format("%s%s", PATH, lockType));
        return !file.exists() || !file.isFile();
    }

    public void lock(String lockType) {
        File file = new File(String.format("%s%s", PATH, lockType));
        try {
            boolean t = file.createNewFile();
            if (t)
                System.out.println("LOCKED " + lockType);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void unlock(String lockType) {
        File file = new File(String.format("%s%s", PATH, lockType));
        boolean t = file.delete();
        if (t)
            System.out.println("UNLOCKED " + lockType);
    }
}
