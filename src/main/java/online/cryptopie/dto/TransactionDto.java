package online.cryptopie.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionDto {
    private String sign;
    private String salt;
    private String note;

    private String amount;
    private String currency;
    private String account_id;
    private String gateId;

    private String fromAccount;
    private String toAccount;

    private String toGate;
    private String fromGate;
    private String toAmount;
    private String fromAmount;
    private String toCurrency;
    private String fromCurrency;

    private String userPhone;
    private String cardNumber;
    private String systemRate;

    private String commission;
    private String finalAmount;
    private String limit;
    private String split_id;

    private String exmo_type;
    private String pair;
    private String quantity;
    private String price;
}
