package online.cryptopie.repositories;

import online.cryptopie.models.Merchant;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MerchantRepository extends MongoRepository<Merchant, ObjectId> {
    Merchant findByAccount(String account);
}
