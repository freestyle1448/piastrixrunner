package online.cryptopie.repositories;

import online.cryptopie.models.Count;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CounterRepository extends MongoRepository<Count, ObjectId> {

}
