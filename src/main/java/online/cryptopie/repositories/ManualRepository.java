package online.cryptopie.repositories;

import online.cryptopie.models.Account;
import online.cryptopie.models.Currency;
import online.cryptopie.models.Gate;
import online.cryptopie.models.transaction.Balance;
import online.cryptopie.models.transaction.Status;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.models.transaction.Type;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static online.cryptopie.app.PiastrixProps.PIASTRIX_GATE_ID;
import static online.cryptopie.models.transaction.Status.IN_PROCESS;
import static online.cryptopie.models.transaction.Status.WAITING;
import static online.cryptopie.models.transaction.Type.USER_OUT;

@Repository
public class ManualRepository {
    private final MongoTemplate mongoTemplate;
    private final CurrenciesRepository currenciesRepository;
    private final GatesRepository gatesRepository;
    private final TransactionsRepository transactionsRepository;

    @Autowired
    public ManualRepository(MongoTemplate mongoTemplate, CurrenciesRepository currenciesRepository, GatesRepository gatesRepository,
                            TransactionsRepository transactionsRepository) {
        this.mongoTemplate = mongoTemplate;
        this.currenciesRepository = currenciesRepository;
        this.gatesRepository = gatesRepository;
        this.transactionsRepository = transactionsRepository;
    }

    public Account findAndModifyAccountSub(String accountId, Balance balance) {
        Query findAndModifyAccount = new Query(Criteria
                .where("accountId").is(accountId)
                .and("balance.amount").gte(balance.getAmount().doubleValue())
                .and("balance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("balance.amount", -balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModifyAccount, update, Account.class);
    }

    public Gate findAndModifyGateSub(ObjectId gateId, Balance balance) {
        Query findAndModifyGate = new Query(Criteria
                .where("_id").is(gateId)
                .and("operationBalance.amount").gte(balance.getAmount().doubleValue())
                .and("operationBalance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("operationBalance.amount", -balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public Account findAndModifyAccountAdd(String accountId, Balance balance) {
        Query findAndModifyAccount = new Query(Criteria
                .where("accountId").is(accountId)
                .and("currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("balance.amount", balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModifyAccount, update, Account.class);
    }

    public Gate findAndModifyGateAdd(ObjectId gateId, Balance balance) {
        Gate gate = gatesRepository.findById(gateId).get();
        Query findAndModifyGate = new Query(Criteria.where("_id").is(gateId)
                .and("operationBalance.currency").is(balance.getCurrency())
                .and("balance.amount").gte(gate.getOperationBalance().getAmount().doubleValue() + balance.getAmount().doubleValue()));
        Update update = new Update();
        update.inc("operationBalance.amount", balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public void findAndUpdateGateCommissionAndAccount(ObjectId gateId, Balance balance) {
        Query findAndModifyGate = new Query(Criteria.where("_id").is(gateId)
                .and("commissionBalance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("commissionBalance.amount", balance.getAmount().doubleValue());

        Optional<Gate> gate = gatesRepository.findById(gateId);

        Query findAndModifyAccount = null;
        if (gate.isPresent())
            findAndModifyAccount = new Query(Criteria.where("accountId").is(gate.get().getAccount())
                    .and("balance.currency").is(balance.getCurrency()));

        Update updateAccount = new Update();
        updateAccount.inc("balance.amount", balance.getAmount().doubleValue());

        assert findAndModifyAccount != null;
        Account accountUpdated = mongoTemplate.findAndModify(findAndModifyAccount, updateAccount, Account.class);
        assert accountUpdated != null;
        Transaction transaction = Transaction.builder()
                .gateId(gateId)
                .date(new Date())
                .accountId(accountUpdated.getAccountId())
                .status(Status.SUCCESS)
                .type(Type.COMMISSION)
                .amount(balance)
                .finalAmount(balance)
                .build();
        Gate updated = mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
        if (updated != null && balance.getAmount().doubleValue() != 0) {
            transactionsRepository.insert(transaction);
        }
    }

    public Currency findBalanceCurrencyByGateId(ObjectId gateId) {
        Optional<Gate> optionalGate = gatesRepository.findById(gateId);
        if (optionalGate.isPresent()) {
            Gate gate = optionalGate.get();

            return currenciesRepository.findByCurrency(gate.getBalance().getCurrency());
        }

        return Currency.builder().build();
    }

    public Transaction findAndModifyPiastrixTransaction() {
        Query query = new Query(Criteria.where("gateId").is(PIASTRIX_GATE_ID)
                .and("status").is(WAITING)
                .and("stage").is(0)
                .and("type").is(USER_OUT));

        Update update = new Update();
        update.set("stage", 1);
        update.set("status", IN_PROCESS);

        return mongoTemplate.findAndModify(query, update, Transaction.class);
    }

    public List<Transaction> findPiastrixTransaction() {
        Query query = new Query(Criteria.where("status").is(IN_PROCESS)
                .and("stage").is(1)
                .and("type").is(USER_OUT)
                .and("gateId").is(PIASTRIX_GATE_ID)
        );

        return mongoTemplate.find(query, Transaction.class);
    }
}
