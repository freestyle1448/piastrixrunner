package online.cryptopie.repositories;

import online.cryptopie.models.Gate;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GatesRepository extends MongoRepository<Gate, ObjectId> {
    Gate findByShopId(Integer shopId);

    Gate findByIdAndBalance_Currency(ObjectId id, ObjectId currency);

    Gate findByIdAndBalance_AmountGreaterThanEqualAndCurrency(ObjectId gateId, Number amount, ObjectId currency);
}

