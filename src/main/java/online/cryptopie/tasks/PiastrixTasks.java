package online.cryptopie.tasks;

import online.cryptopie.app.Locker;
import online.cryptopie.models.transaction.History;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.repositories.TransactionsRepository;
import online.cryptopie.services.PiastrixService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static online.cryptopie.app.Application.HASH_PAS;
import static online.cryptopie.app.Locker.*;
import static online.cryptopie.models.transaction.AmountTypes.OUT;
import static online.cryptopie.models.transaction.Status.IN_PROCESS;
import static online.cryptopie.models.transaction.Status.WAITING;

@Component
@Async
public class PiastrixTasks {
    private final Locker locker = Locker.getInstance();
    private final ManualRepository manualRepository;
    private final PiastrixService piastrixService;
    private final TransactionsRepository transactionsRepository;

    public PiastrixTasks(ManualRepository manualRepository, PiastrixService piastrixService, TransactionsRepository transactionsRepository) {
        this.manualRepository = manualRepository;
        this.piastrixService = piastrixService;
        this.transactionsRepository = transactionsRepository;
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte aHash : hash) {
            String hex = Integer.toHexString(0xff & aHash);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static String genHash(Transaction transaction) {
        if (transaction.getTransactionNumber() == null)
            transaction.setTransactionNumber(0L);

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert digest != null;

        byte[] encodedhash = digest.digest(
                String.format("%s|%s|%s|%s|%s",
                        transaction.getCardNumber(), transaction.getDate(),
                        transaction.getAmount().getAmount().doubleValue(),
                        transaction.getFinalAmount().getAmount().doubleValue(), HASH_PAS).getBytes(StandardCharsets.UTF_8));

        return bytesToHex(encodedhash);
    }

    @SuppressWarnings("Duplicates")
    @Scheduled(fixedRate = 10000)
    public void withdrawScheduled() {
        if (locker.isNotLocked(PING_LOCK) && locker.isNotLocked(PAY_LOCK) && locker.isNotLocked(CONFIRM_LOCK)) {
            locker.lock(PAY_LOCK);
            List<Transaction> transactions = new ArrayList<>();
            Transaction tr = manualRepository.findAndModifyPiastrixTransaction();
            while (tr != null) {
                transactions.add(tr);
                tr = manualRepository.findAndModifyPiastrixTransaction();
            }

            transactions.parallelStream().forEach(transaction -> {
                if (transaction.getHistoryList() == null) {
                    transaction.setHistoryList(new ArrayList<>());
                    transaction.getHistoryList()
                            .add(History.builder()
                                    .date(new Date())
                                    .prevStatus(WAITING)
                                    .curStatus(IN_PROCESS)
                                    .build());
                }
                transaction.setAmountType(OUT);
                transaction.setStatus(IN_PROCESS);
                transaction.setStage(1);

                if (transaction.getHash().equals(genHash(transaction))) {
                    piastrixService.saveTransaction(transaction);
                    try {
                        piastrixService.withdraw(transaction);
                    } catch (ResourceAccessException ex) {
                        locker.unlock(PAY_LOCK);
                    }

                } else {
                    piastrixService.declineTransaction(transaction, "Неверный hash!");
                }
            });
        }

        locker.unlock(PAY_LOCK);
    }

    @SuppressWarnings("Duplicates")
    @Scheduled(fixedRate = 10000, initialDelay = 500)
    public void checkTransactions() {
        if (locker.isNotLocked(PING_LOCK) && locker.isNotLocked(PAY_LOCK) && locker.isNotLocked(CONFIRM_LOCK)) {
            locker.lock(CONFIRM_LOCK);

            List<Transaction> transactions = manualRepository.findPiastrixTransaction();

            transactions.parallelStream().forEach(transaction -> {
                if (transaction.getHash().equals(genHash(transaction))) {
                    piastrixService.saveTransaction(transaction);
                    try {
                        piastrixService.checkTransaction(transaction);
                    } catch (ResourceAccessException ex) {
                        locker.unlock(CONFIRM_LOCK);
                    }
                } else {
                    piastrixService.declineTransaction(transaction, "Неверный hash!");
                }
            });
            locker.unlock(CONFIRM_LOCK);
        }
    }


    @Scheduled(fixedRate = 60000, initialDelay = 1000)
    public void ping() {
        if (locker.isNotLocked(PING_LOCK) && locker.isNotLocked(PAY_LOCK) && locker.isNotLocked(CONFIRM_LOCK)) {
            locker.lock(PING_LOCK);
            try {
                piastrixService.ping();
            } catch (ResourceAccessException ex) {
                locker.unlock(PING_LOCK);
            }
        }
    }
}
