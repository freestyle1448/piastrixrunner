package online.cryptopie.tasks;

import online.cryptopie.models.piastrix.withdraw.DataWithDraw;
import online.cryptopie.models.transaction.Balance;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.CurrenciesRepository;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.services.PiastrixService;
import org.bson.types.ObjectId;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

import static online.cryptopie.models.transaction.AmountTypes.OUT;
import static online.cryptopie.models.transaction.Type.USER_OUT;

@RestController
public class Test {
    private final ManualRepository manualRepository;
    private final PiastrixService piastrixService;
    private final CurrenciesRepository currenciesRepository;

    public Test(ManualRepository manualRepository, PiastrixService piastrixService, CurrenciesRepository currenciesRepository) {
        this.manualRepository = manualRepository;
        this.piastrixService = piastrixService;
        this.currenciesRepository = currenciesRepository;
    }

    @Async
    @GetMapping("/")
    public void test() {
        final Transaction eur = Transaction.builder()
                .finalAmount(Balance.builder()
                        .currency("RUB")
                        .amount(12.34)
                        .build())
                .type(USER_OUT)
                .date(new Date())
                .data(DataWithDraw.builder()
                        .id(11298076)
                        .build())
                .amountType(OUT)
                .gateId(new ObjectId("5cd96d62f64edb30dc0097c4"))
                .build();

        eur.getFinalAmount().setCurrencyObj(currenciesRepository.findByCurrency(eur.getFinalAmount().getCurrency()));
        piastrixService.ping();
    }
}
