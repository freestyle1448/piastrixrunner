package online.cryptopie.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.models.transaction.Balance;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "accounts")
public class Account {
    @Id
    private ObjectId id;
    private Balance balance;
    private ObjectId userId;
    private String currency;
    private Integer incrementId;
    private String accountId;
    private Overdraft overdraft;
}
