package online.cryptopie.models.transaction;

public final class AmountTypes {
    public static final String IN = "ps_amount";
    public static final  String OUT = "shop_amount";
}
