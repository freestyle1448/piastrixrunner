package online.cryptopie.models.transaction;

import lombok.*;
import online.cryptopie.models.Currency;
import online.cryptopie.repositories.CurrenciesRepository;
import org.javamoney.moneta.CurrencyUnitBuilder;
import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;
import javax.money.UnknownCurrencyException;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString

@Component
public class Balance {
    private Number amount;
    private String currency;
    private Currency currencyObj;
    private CurrenciesRepository currenciesRepository;

    @Autowired
    public Balance(CurrenciesRepository currenciesRepository) {
        this.currenciesRepository = currenciesRepository;
    }

    public static MonetaryAmount getMonetary(Balance balance) {
        MonetaryAmount money;

        try {
            money = Money.of(balance.getAmount().doubleValue(), balance.getCurrency());
        } catch (UnknownCurrencyException e) {
            Currency c = balance.getCurrencyObj();
            CurrencyUnit cur = CurrencyUnitBuilder.of(balance.getCurrencyObj().getCurrency(), "default")
                    .setNumericCode(c.getCode())
                    .setDefaultFractionDigits(3)
                    .build();
            money = Money.of(balance.getAmount().doubleValue(), cur);
        }

        return money;
    }

    public static double add(Balance addend1, Balance addend2) {
        MonetaryAmount addend_ma1, addend_ma2;
        addend_ma1 = getMonetary(addend1);
        addend_ma2 = getMonetary(addend2);
        return addend_ma1.add(addend_ma2).getNumber().doubleValueExact();
    }

    public static Balance sub(Balance addend1, Balance addend2) {
        MonetaryAmount addend_ma1, addend_ma2;
        addend_ma1 = getMonetary(addend1);
        addend_ma2 = getMonetary(addend2);

        return getBalance(addend_ma1.subtract(addend_ma2), addend1.getCurrencyObj());
    }

    private static Balance getBalance(MonetaryAmount amount, Currency currency) {
        return Balance.builder()
                .amount(amount.getNumber().doubleValue())
                .currency(currency.getCurrency())
                .currencyObj(currency)
                .build();
    }
}
