package online.cryptopie.models.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.dto.TransactionDto;
import org.bson.types.ObjectId;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class TransactionWrapper {
    private ObjectId gateId;
    private List<TransactionDto> transactions;
}
