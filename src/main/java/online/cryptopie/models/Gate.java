package online.cryptopie.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.models.transaction.Balance;
import online.cryptopie.models.transaction.Commission;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "gates")
public class Gate {
    @Id
    private ObjectId id;
    private String name;
    private Integer type;
    private String currency;
    private Commission commission;
    private Balance commissionBalance;
    private Balance balance;
    private Balance operationBalance;
    private String account;
    @Field("shop_id")
    private Integer shopId;
}
