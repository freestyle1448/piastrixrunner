package online.cryptopie.models.piastrix.withdraw;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.app.PiastrixProps;
import online.cryptopie.app.Tools;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private String account;
    private Double amount;
    private String amount_type;
    private String payway;
    private Integer shop_currency;
    private Integer shop_id;
    private String shop_payment_id;
    private String sign;

    public void genSign() {
        final String originalString = account + ":" + amount + ":" + amount_type + ":" + payway + ":" + shop_currency
                + ":" + shop_id + ":" + shop_payment_id + PiastrixProps.SECRET_KEY;

        this.sign = Tools.genSign(originalString);
    }
}
