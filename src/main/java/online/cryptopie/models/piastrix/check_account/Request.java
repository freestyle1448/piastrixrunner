package online.cryptopie.models.piastrix.check_account;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.app.PiastrixProps;
import online.cryptopie.app.Tools;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private String account;
    private AccountDetails account_details;
    private String sign;
    private String payway;
    private Double amount;
    private Integer shop_id;

    public void setSign() {
        final String originalString = account + ":" + amount + ":" + payway + ":" + shop_id + PiastrixProps.SECRET_KEY;

        this.sign = Tools.genSign(originalString);
    }
}
