package online.cryptopie.models.piastrix.check_account;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DataCheck {
    private AccountInfo account_info;
    private Integer provider_status;
    private Boolean result;
}
