package online.cryptopie.models.piastrix.shop_balance;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.app.PiastrixProps;
import online.cryptopie.app.Tools;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private String now;
    private Integer shop_id;
    private String sign;

    public void genSign() {
        final String originalString = now + ":" + shop_id + PiastrixProps.SECRET_KEY;

        this.sign = Tools.genSign(originalString);
    }
}
