package online.cryptopie.models.piastrix.withdraw_try;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.app.PiastrixProps;
import online.cryptopie.app.Tools;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Request {
    private String amount_type;
    private Integer shop_currency;
    private String payway;
    private Double amount;
    private Integer shop_id;
    private String sign;

    public void genSign() {
        final String originalString = amount + ":" + amount_type + ":" + payway + ":" + shop_currency + ":" + shop_id + PiastrixProps.SECRET_KEY;

        this.sign = Tools.genSign(originalString);
    }
}
