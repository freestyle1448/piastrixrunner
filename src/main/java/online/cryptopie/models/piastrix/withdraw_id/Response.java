package online.cryptopie.models.piastrix.withdraw_id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Response {
    private DataWithDrawId data;
    private Integer errorCode;
    private String message;
    private Boolean result;
}
